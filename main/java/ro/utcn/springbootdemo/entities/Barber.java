package ro.utcn.springbootdemo.entities;

import com.google.common.base.Objects;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Barber {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;

    @Column
    private String title;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "barber_request",
            joinColumns = @JoinColumn(name = "barber_id"),
            inverseJoinColumns = @JoinColumn(name = "request_id"))
    private Set<Request> requests = new HashSet<>();

    public boolean add(Request request)
    {
        request.getBarberList().add(this);
        return requests.add(request);
    }

    public boolean remove(Request request)
    {
        request.getBarberList().remove(this);
        return requests.remove(request);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Barber barber = (Barber) o;
        return Objects.equal(id, barber.id) &&
                Objects.equal(title, barber.title);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(id, title);
    }
}
