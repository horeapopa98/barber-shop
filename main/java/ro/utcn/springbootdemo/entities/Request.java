package ro.utcn.springbootdemo.entities;

import com.google.common.base.Objects;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Request {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;

    @Column(nullable = false, unique = true)
    @NaturalId
    private String name;

    @ManyToMany(mappedBy = "requests")
    private Set<Barber> barberList = new HashSet<>();

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Request request = (Request) o;
        return Objects.equal(id, request.id) &&
                Objects.equal(name, request.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(id, name);
    }
}
