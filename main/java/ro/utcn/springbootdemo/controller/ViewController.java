/*************************************************************************
 * ULLINK CONFIDENTIAL INFORMATION
 * _______________________________
 *
 * All Rights Reserved.
 *
 * NOTICE: This file and its content are the property of Ullink. The
 * information included has been classified as Confidential and may
 * not be copied, modified, distributed, or otherwise disseminated, in
 * whole or part, without the express written permission of Ullink.
 ************************************************************************/
package ro.utcn.springbootdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ro.utcn.springbootdemo.dto.BarberRequestDTO;
import ro.utcn.springbootdemo.entities.Barber;
import ro.utcn.springbootdemo.service.RequestsService;

@Controller
public class ViewController {

    @Autowired
    private RequestsService requestsService;

    @RequestMapping({"/home"})
    public String home(Model model) {
        List<Barber> allBarbers = requestsService.getAllBarbers();
        model.addAttribute("barbers", allBarbers);
        return "home";
    }

    @RequestMapping(value="/user", method = RequestMethod.GET)
    public String user(Model model){
        List<Barber> allBarbers = requestsService.getAllBarbers();
        model.addAttribute("barbers", allBarbers);
        return "user";
    }

    @RequestMapping(path = "/details/{requestId}", produces = "text/html")
    public String details(@PathVariable("requestId") long requestId, Model model) {
        Barber barber = requestsService.getById(requestId);
        model.addAttribute("barber", barber);
        model.addAttribute("requests", barber.getRequests());
        model.addAttribute("newMapping", new BarberRequestDTO(barber, ""));
        return "barber_details";
    }

    @RequestMapping(value = "/createTag", method = RequestMethod.POST)
    public String createTag(@ModelAttribute BarberRequestDTO newMapping) {
        requestsService.addOrCreateRequest(newMapping.getBarber(), newMapping.getRequestName());
        Long barberId = newMapping.getBarber().getId();
        return "redirect:/details/" + barberId;
    }

    @RequestMapping(value = "/removeTag", method = RequestMethod.POST)
    public String removeTag(@ModelAttribute BarberRequestDTO newMapping) {
        requestsService.removeRequest(newMapping.getBarber(), newMapping.getRequestName());
        Long barberId = newMapping.getBarber().getId();
        return "redirect:/details/" + barberId;
    }
}