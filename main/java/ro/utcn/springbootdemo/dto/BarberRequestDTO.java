package ro.utcn.springbootdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ro.utcn.springbootdemo.entities.Barber;

@Data
@AllArgsConstructor
public class BarberRequestDTO {
    private Barber barber;
    private String requestName;
}
