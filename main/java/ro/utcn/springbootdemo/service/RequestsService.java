package ro.utcn.springbootdemo.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.springbootdemo.entities.Barber;
import ro.utcn.springbootdemo.entities.Request;
import ro.utcn.springbootdemo.repository.BarberRepository;
import ro.utcn.springbootdemo.repository.RequestRepository;

import java.util.List;

@Service
public class RequestsService {
    @Autowired
    private BarberRepository repository;
    @Autowired
    private RequestRepository requestRepository;

    public List<Barber> getAllBarbers()
    {
        return Lists.newArrayList(repository.findAll());
    }

    public Barber getById(long id)
    {
        return repository.findOneWithTagsById(id).orElseThrow(() -> new IllegalArgumentException("Barber not found. Id:" + id));
    }

    public void addOrCreateRequest(Barber barber, String requestName)
    {
        Request request = requestRepository.findOneByName(requestName).orElseGet(() -> createRequest(requestName));
        barber.add(request);
        repository.save(barber);
    }

    public void removeRequest(Barber barber, String requestName)
    {
        Request request = requestRepository.findOneByName(requestName).orElseGet(() -> createRequest(requestName));
        barber.remove(request);
        repository.save(barber);
    }

    private Request createRequest(String requestName)
    {
        Request request = new Request();
        request.setName(requestName);
        return request;
    }
}
