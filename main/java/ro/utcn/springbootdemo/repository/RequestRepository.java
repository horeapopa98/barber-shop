package ro.utcn.springbootdemo.repository;

import org.springframework.data.repository.CrudRepository;
import ro.utcn.springbootdemo.entities.Request;

import java.util.Optional;

public interface RequestRepository extends CrudRepository<Request, Long> {
    Optional<Request> findOneByName(String name);
}
