package ro.utcn.springbootdemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ro.utcn.springbootdemo.entities.Barber;

import java.util.Optional;

public interface BarberRepository extends CrudRepository<Barber, Long> {
    Optional<Barber> findOneWithTagsById(@Param("id") Long id);
}
