package ro.utcn.springbootdemo.repository;

import org.springframework.data.repository.CrudRepository;
import ro.utcn.springbootdemo.entities.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);
}