package ro.utcn.springbootdemo.repository;

import org.springframework.data.repository.CrudRepository;
import ro.utcn.springbootdemo.entities.Role;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findByRole(String role);

}